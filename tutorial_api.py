
# API
'''
    API - Application Programming Interface

    Methods of 
    Communication 
    Between 
    Software 
    Components

    API - fundamental building programs 

    Internal                External
    software for            connect            
    web-camera              with another  
    local things            service
    of computer             e.g: authentification 
                            with facebook 
'''

# API application
'''
    1) Web Based
    2) OS 
    3) Database
    4) Hardware 
    5) Libraries 
'''

# Examples 
'''
    Django - Using built-in class based views
    Stripe - Running a transaction 
'''

# REST API - web based veriosn of API 
# REpresentation State Transfer
'''
    Methods to Access Process and Handle Web Resources

    URI - uniform resource identifier, link to a web-given resource 
    does not have to be a document, can be an endpoint, similar to URL, 
    URL is general but URI is very specific 

'''

# HTTP 
'''
    HTTP - POST (create), PUT (update), GET (retrieve), DELETE (delete)
    
    Software Design 
    CRUD (Create Retrieve Update Delete) - fundamental 
    concept of web applications
    each location of some app does kind of this stuff 

    we use HTTP to do CRUD 
'''










