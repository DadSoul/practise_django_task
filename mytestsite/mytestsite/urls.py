"""mytestsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.views.generic import RedirectView

# for serving static files during development 
from django.conf import settings 
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += [
    url('catalog/', include('catalog.urls')),
]

'''
permanent
    Whether the redirect should be permanent. 
    The only difference here is the HTTP status code returned. 
    If True, then the redirect will use status code 301. 
    If False, then the redirect will use status code 302. 
    By default, permanent is False.
'''

urlpatterns += [
    url('', RedirectView.as_view(url = 'catalog/', permanent = True))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) 



