from django.contrib import admin

# Register your models here.
from .models import Update as UpdateModel


'''
    Finally, determine which of your application’s models
    should be editable in the admin interface. 
    For each of those models, register them with 
    the admin as described in ModelAdmin.
'''

admin.site.register(UpdateModel)
  