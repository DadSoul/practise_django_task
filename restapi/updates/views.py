import json


# importing a base View, all other views inherit it
from django.views.generic import View

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
# Create your views here.
from .models import Update
from restapi.mixins import JsonResponseMixin
from django.core.serializers import serialize

# def detail_view(request): 
#     return render() # return JSON data 
    # render is shortcut for this method call
    # return HttpResponse(get_template().render({}))



def json_example_view(request):
    '''
        URI -- for a REST API  
        GET -- Retrieve
    '''
    data = {
        "count":1000, 
        "content":"Hello World"
    }

    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type = "application/json")
    # return JsonResponse(data) -- same as above 

class JsonCBV(View):
    def get(self, request, *args, **kwargs): 
        '''
            URI -- for a REST API  
            GET -- Retrieve
        '''
        data = {
            "count":1000, 
            "content":"Hello World"
        }
        json_data = json.dumps(data)
        return HttpResponse(json_data, content_type = "application/json")

class JsonCBV2(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        data = {
            "count":1000, 
            "content":"Hello World"
        }
        return self.render_to_json_response(data)
    
class SerializedDetailView(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        obj = Update.objects.get(id = 1)
        json_data = obj.serialize() 
        return HttpResponse(json_data, content_type = "application/json")
  
class SerializedListView(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        qs = Update.objects.all()
        # convert some data type to dictionary 
        json_data = Update.objects.all().serialize()
        return HttpResponse(json_data , content_type = "application/json")
  

'''
    notes: 
    reason for using JSON 
    simple and is not messa as XML 
    uniform datatype 
    looks like python dictionary 
'''