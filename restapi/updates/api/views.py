from updates.models import Update as UpdateModel 
from updates.forms import UpdateModelForm

from django.views.generic import View 
from django.http import HttpResponse

from restapi.mixins import HttpResponseMixin
from .mixin import CSRFExemptMixin
from .utility import is_json
import json


# /api/updates - one endpoint for CRUD 


class UpdateModelDetailAPIView(HttpResponseMixin, CSRFExemptMixin, View): 
    '''
        Retrieve, Update, Delete -> Object 
    '''
    is_json = True

    def get_object(self, id = None): 
        # one method of handling the getting element
        # try: 
        #     obj = UpdateModel.objects.get(id = id)
        # except UpdateModel.DoesNotExist:
        #     obj = None 
        # another method of handling the getting element
        
        qs = UpdateModel.objects.filter(id = id)
        if qs.count() == 1: 
            return qs.first() 
        return None

    def get(self, request, id, *args, **kwargs): 
        obj = self.get_object(id = id)
        if obj is None: 
            error_data = json.dumps({"message":"Update not found"})
            return self.render_to_response(error_data, status=404)
        json_data = obj.serialize()
        return self.render_to_response(json_data)

    def post(self, request, *args, **kwargs):
        data = {"message": "Not allowed, please use the /api/updates/ endpoint"}
        json_data = json.dumps(data)
        return self.render_to_response(json_data, status = 403) # Forbidden
  
    def put(self, request, id, *args, **kwargs):
        
        valid_json = is_json(request.body)

        if not valid_json: 
            error_data = json.dumps({"message":"Invalid data sent, please send using JSON"})
            return self.render_to_response(error_data, status=400) # BAD REQUEST

        obj = self.get_object(id = id)

        if obj is None: 
            error_data = json.dumps({"message":"Update not found"})
            return self.render_to_response(error_data, status=404)
        
        data = json.loads(obj.serialize())
        passed_data = json.loads(request.body)
        # updating attributes 
        for key, value in passed_data.items(): 
            data[key] = value
        # most of the time how we update the object
        form = UpdateModelForm(data, instance = obj)

        if form.    is_valid(): 
            obj = form.save(commit = True)
            obj_data = json.dumps(data) #obj.slize() 
            return self.render_to_response(obj_data, status = 201)
        
        if form.errors:
            data = json.dumps(form.errors)
            return self.render_to_response(data, status = 400)

        new_data = json.loads(request.body)
        print(new_data['content'])
        json_data = json.dumps({"message":"Something"})
        return self.render_to_response(json_data, status = 201)

    def delete(self, request, id,*args, **kwargs):
        obj = self.get_object(id = id)
        if obj is None: 
            error_data = json.dumps({"message":"Update not found"})
            return self.render_to_response(error_data, status=404)
        obj_deleted_num, dict_specifics = obj.delete()
        if obj_deleted_num == 1: 
            status_code = 200
            json_data = json.dumps({"message":"Deleted succefully"})
        else:
            status_code = 400
            json_data = json.dumps({"message":"Could not normally delete"})
        return self.render_to_response(json_data, status=status_code)


# AUTH / Permissions -- DJANGO REST FRAMEWORK 

class UpdateModelListAPIView(HttpResponseMixin, CSRFExemptMixin, View): 
    '''
        List View --> Retrieve
        Create View  
        Update 
        Delete
    '''

    is_json = True
    queryset = None 

    def get_queryset(self):
        qs = UpdateModel.objects.all()
        self.queryset = qs
        return qs

    def get_object(self, id = None): 

        if id is None: 
            return None

        # one method of handling the getting element
        # try: 
        #     obj = UpdateModel.objects.get(id = id)
        # except UpdateModel.DoesNotExist:
        #     obj = None 
        # another method of handling the getting element

        if self.queryset is None: 
            self.queryset = self.get_queryset()

        qs = self.queryset.filter(id = id)
        if qs.count() == 1: 
            return qs.first() 
        return None
    

    def get(self, request, *args, **kwargs): 
        print("UpdateModelListAPIView GET method was called")
        data = json.loads(request.body)
        passed_id = data.get("id", None)
        # if it has id 
        if passed_id is not None: 
            obj = self.get_object(id = passed_id)
            if obj is None: 
                error_data = json.dumps({"message":"Object not found"})
                return self.render_to_response(error_data, status=404)
            json_data = obj.serialize()
            return self.render_to_response(json_data)
        else:
            qs = self.get_queryset()
            json_data = qs.serialize()
            return self.render_to_response(json_data)
 
    def post(self, request, *args, **kwargs):
        valid_json = is_json(request.body)
        if not valid_json: 
            error_data = json.dumps({"message":"Invalid data sent, please send using JSON"})
            return self.render_to_response(error_data, status=400) # BAD REQUEST
        
        data = json.loads(request.body)
        form = UpdateModelForm(data)
        if form.is_valid(): 
            obj = form.save(commit = True)
            obj_data = obj.serialize() 
            return self.render_to_response(obj_data, status = 201)
        if form.errors:
            data = json.dumps(form.errors)
            return self.render_to_response(data, status = 400)
        data = {"Message":"Not Allowed"}
        # return HttpResponse(data, content_type = 'application/json', status = 201)
        return self.render_to_response(data, status=400)

    def put(self, request, *args, **kwargs):
        
        valid_json = is_json(request.body)

        if not valid_json: 
            error_data = json.dumps({"message":"Invalid data sent, please send using JSON"})
            return self.render_to_response(error_data, status=400) # BAD REQUEST

        passed_data = json.loads(request.body)
        passed_id = passed_data.get('id', None)

        print("PASSED_ID IS {}".format(passed_id))

        if passed_id is None: 
            error_data = json.dumps({"messsage": "id attribute is required for updating"})
            return self.render_to_response(error_data, status = 400) # BAD REQUEST

        obj = self.get_object(id = passed_id)

        if obj is None: 
            error_data = json.dumps({"message":"Object not found"})
            return self.render_to_response(error_data, status=404)  # NOT FOUND
        
        data = json.loads(obj.serialize())
        # updating attributes 
        for key, value in passed_data.items(): 
            data[key] = value
        # most of the time how we update the object
        form = UpdateModelForm(data, instance = obj)

        if form.is_valid(): 
            obj = form.save(commit = True)
            obj_data = json.dumps(data) #obj.serialize() 
            return self.render_to_response(obj_data, status = 201)
        
        if form.errors:
            data = json.dumps(form.errors)
            return self.render_to_response(data, status = 400)

        new_data = json.loads(request.body)
        print(new_data['content'])
        json_data = json.dumps({"message":"Something"})
        return self.render_to_response(json_data, status = 201)

    def delete(self, request ,*args, **kwargs):
        valid_json = is_json(request.body)

        if not valid_json: 
            error_data = json.dumps({"message":"Invalid data sent, please send using JSON"})
            return self.render_to_response(error_data, status=400) # BAD REQUEST

        passed_data = json.loads(request.body)
        passed_id = passed_data.get('id', None)

        if passed_id is None: 
            error_data = json.dumps({"messsage": "id attribute is required for updating"})
            return self.render_to_response(error_data, status = 400) # BAD REQUEST

        obj = self.get_object(id = passed_id)
        if obj is None: 
            error_data = json.dumps({"message":"Object not found"})
            return self.render_to_response(error_data, status=404)
       
        obj_deleted_num, dict_specifics = obj.delete()
        if obj_deleted_num == 1: 
            status_code = 200
            json_data = json.dumps({"message":"Deleted succefully"})
        else:
            status_code = 400
            json_data = json.dumps({"message":"Could not normally delete"})
        return self.render_to_response(json_data, status=status_code)

# not recommended to create split class for each split REST method 


