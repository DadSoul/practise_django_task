from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

# Mixins are a form of multiple inheritance where behaviors 
# and attributes of multiple parent classes can be combined.

class CSRFExemptMixin(object): 
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs) 
