from django.db import models
from django.conf import settings
from django.core.serializers import serialize
import json

# Create your models here.

def upload_update_image(instance, filename):
    return "updates/{user}/{filename}".format(user = instance.user, filename = filename)

class UpdateManager(models.Manager): 
    def get_queryset(self):
        return UpdateQuerySet(self.model, using = self._db)


class UpdateQuerySet(models.QuerySet):
    def serialize(self):
        qs = list(self.values("user", "content", "image", "id"))
        # final_array = [] 
        # for obj in qs: 
        #     to_dict_obj = json.loads(obj.serialize()) 
        #     final_array.append(to_dict_obj) 
        return json.dumps(qs) #serialize('json',  qs, fields = ('user', 'content','image'))

class Update(models.Model):
    # one-to-many relation 
    user        = models.ForeignKey(settings.AUTH_USER_MODEL)
    content     = models.TextField(blank = True, null= True) 
    # just ignore for now
    image       = models.ImageField(upload_to = upload_update_image, blank = True, null = True)
    # auto_now_add (to only set the date when the model is first created) 
    timestamp   = models.DateTimeField(auto_now_add=True)
    # auto_now=True (to set the field to the current date every time the model is saved),
    updated     = models.DateTimeField(auto_now=True)

    objects = UpdateManager() 

    def __str__(self):
        return self.content or ""

    def serialize(self):

        try: 
            image = self.image.url
        except: 
            image = ""
        
        data = {
            "content": self.content, 
            "user": self.user.id, 
            "image": image, 
            "id": self.user.id
        }
        return json.dumps(data)

