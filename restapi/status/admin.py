from django.contrib import admin
from .models import Status as StatusModel
from .forms import StatusForm
# Register your models here.

class StatusAdmin(admin.ModelAdmin):
    # Set list_display to control which fields are displayed on the change list page of the admin.
    list_display= ['user', '__str__', 'image']
    form = StatusForm

admin.site.register(StatusModel, StatusAdmin)
