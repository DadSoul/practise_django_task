from django.test import TestCase
from django.contrib.auth import get_user_model
from .models import Status

User = get_user_model()



class StatusTestCase(TestCase):
    
    def setUp(self):
        user = User.objects.create(username = 'DDDAA', email = "dfads@gmail.com")
        user.set_password('123abcde')
        user.save()
    
    def test_create_status(self):
        user    = User.objects.get(username = 'DDDAA')
        obj     = Status.objects.create(user = user, content = "some new content")
        self.assertEqual(obj.id, 1)
        qs      = Status.objects.all()
        self.assertEqual(qs.count(), 1) 






















