from django.db import models
from django.conf import settings

def upload_image_to(instance, filename):
    return "status/{user}/{filename}".format(user = instance.user, filename = filename)

class StatusQuerySet(models.QuerySet):
    pass

class StatusManager(models.Manager):
    def get_queryset(self):
        return StatusQuerySet(self.model, using=self._db)


class Status(models.Model): # fb status, instagram post, linkedin post 
    user            = models.ForeignKey(settings.AUTH_USER_MODEL)
    content         = models.TextField(null = True, blank=True)
    image           = models.ImageField(upload_to = upload_image_to, null = True, blank = True)
    # time of initial creation
    timestamp       = models.DateTimeField(auto_now_add=True)
    # updates time every time when object is updates
    updated         = models.DateTimeField(auto_now=True)

    objects = StatusManager()

    def __str__(self): 
        return str(self.user.username[:50])

    # how would they look like in database
    class Meta: 
        verbose_name = "Status post"
        verbose_name_plural = "Status posts"


    # for hadnling permissions
    @property 
    def owner(self):
        return self.user

    

