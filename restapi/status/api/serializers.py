from rest_framework import serializers
from status.models import Status
from accounts.api.serializers import UserPublicSerializer
from accounts.api.user.serializers import UserDetailSerializer
from rest_framework.reverse import reverse as api_reverse
'''
    Serializers -> JSON 
    Serializers -> validate data
'''

# class CustomSerializer(serializers.Serializer):
#     content         = serializers.CharField()
#     email           = serializers.EmailField()


class StatusSerializer(serializers.ModelSerializer):
    user                = serializers.SerializerMethodField(read_only = True)#UserDetailSerializer(read_only = True)
    uri                 = serializers.SerializerMethodField(read_only = True)
    # user_id             = serializers.HyperlinkedRelatedField(source = 'user', lookup_field = 'username', view_name = 'api-user:detail',read_only = True)
    # user                = serializers.SlugRelatedField(read_only = True, slug_field = 'username')
    
    class Meta: 
        model = Status
        fields = [
            'uri',
            'id', 
            'user',
            'content',
            'image',
            # 'username',
            # 'user_id'
        ]
        read_only_fields = ['user'] # GET 
  
    def get_user(self, obj):
        request = self.context.get('request')
        return UserPublicSerializer(obj.user, read_only = True, context = {'request':request}).data
        #return str(obj.user)

    def get_uri(self, obj):
        return api_reverse('api-status:detail', kwargs = {'id':obj
        .id}, request = self.context.get('request'))
        #return f'api/status/{obj.id}/'

    def validate_content(self, value):
        print(value)
        if value is None: 
            raise serializers.ValidationError("The content must exist")
        if len(value) > 100: 
            raise serializers.ValidationError("This is too long")
        return value

    def validate(self, data):
        content = data.get('content', None)
        if content == "":
            content = None 
        image = data.get("image", None)
        if content is None and image is None: 
            raise serializers.ValidationError("content or image is required")
        return data


