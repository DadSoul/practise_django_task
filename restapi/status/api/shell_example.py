from status.api.serializers import StatusSerializer
from status.models import Status


'''
    Create Object
'''


def create_obj(user = 1, content = "please delete me"):
    data = {'user': user, 'content':content}
    serializer = StatusSerializer(data = data)
    if serializer.is_valid():
        created_object = serializer.save()
        return created_object
    else: 
        return serializer.error_messages

    
'''
    Update Object
'''

# obj = Status.objects.first() 
# data = {'content': 'some new content', 'user': 1}
# serializer = StatusSerializer(obj, data = data)
# serializer.is_valid()
# serializer.save()

'''
    Delete Object
'''
def delete_obj(id = 1):
    to_be_deleted_object = Status.objects.get(id = id)
    to_be_deleted_object.delete()

created_object = create_obj(content=None)
delete_obj(int(created_object.id))

