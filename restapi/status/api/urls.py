"""restapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from .views import (
    StatusAPIView, 
    StatusAPIDetailView,
    ) 
 
urlpatterns = [
    url(r'^$', StatusAPIView.as_view(), name = 'list'),
    url(r'^(?P<id>\d+)/$', StatusAPIDetailView.as_view(), name='detail'),
]

# Start with 
# /api/status -> List 
# /api/status/create -> Create 
# /api/status/12/ -> Detail 
# /api/status/12/update/ -> Update 
# /api/status/12/delete/ -> Delete
# End with 

# Final 
# /api/status/ -> List -> CRUD & LS 
# /api/status/1/ -> Detail -> CRUD 
