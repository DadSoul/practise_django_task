from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status
from status.models  import Status
from django.conf import settings
from PIL import Image
import os
import tempfile
import shutil 

User = get_user_model()

# Create your tests here.
class StatusAPITestCase(APITestCase):
    
    def setUp(self):
        user = User.objects.create(username = 'DDDAA', email = "dfads@gmail.com")
        user.set_password('123abcde')
        user.save()
        status_obj = Status.objects.create(user=user, content = 'Hello There')
    
    def test_statuses(self):
        qs = Status.objects.all()
        self.assertEqual(qs.count(), 1)

    def status_user_token(self):
        auth_url  = api_reverse('api-auth:login')
        auth_data = {
            'username': 'DDDAA',
            'password': '123abcde'
        } 
        auth_response     = self.client.post(auth_url, auth_data, format = 'json')
        token        = auth_response.data.get("token")
        self.client.credentials(HTTP_AUTHORIZATION = 'JWT ' + token)

    def test_users(self):
        qs   = User.objects.all()
        self.assertEqual(qs.count(), 1)
        qs   = qs.filter(username = 'DDDAA')
        self.assertEqual(qs.count(), 1)
    
    def test_status_create(self):
        auth_url  = api_reverse('api-auth:login')
        auth_data = {
            'username': 'DDDAA',
            'password': '123abcde'
        } 
        auth_response     = self.client.post(auth_url, auth_data, format = 'json')
        token        = auth_response.data.get("token")
        self.client.credentials(HTTP_AUTHORIZATION = 'JWT ' + token)
        data = {
            'content':'Some cool test content'
        }
        url = api_reverse('api-status:list')
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Status.objects.count(), 2)
    
    def create_item(self):
        self.status_user_token()
        data = {
            'content':'Some cool test content'
        }
        url = api_reverse('api-status:list')
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        return response.data

    def test_get_created_status(self):
        data = self.create_item()
        data_id  = data.get("id")
        put_url  = api_reverse('api-status:detail', kwargs={"id":data_id})
        rud_response = self.client.get(put_url, format = 'json')
        self.assertEqual(rud_response.status_code, status.HTTP_200_OK)
     
    def test_status_create_with_image(self):
        self.status_user_token()
        image_item      = Image.new('RGB', (800, 1290), (0, 124, 174))
        temp_file       = tempfile.NamedTemporaryFile(suffix = '.jpg')
        image_item.save(temp_file, format = 'JPEG')
        with open(temp_file.name, 'rb') as file_obj:
            data = {
                'content':'Some cool test content',
                'image':file_obj
            }
            url = api_reverse('api-status:list')
            response = self.client.post(url, data, format = 'multipart')
            print(response.data)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        temp_img_dir = os.path.join(settings.MEDIA_ROOT, 'status', 'DDDAA')
        if os.path.exists(temp_img_dir):
            shutil.rmtree(temp_img_dir)
    
    def test_status_create_with_image_no_description(self):
        self.status_user_token()
        image_item      = Image.new('RGB', (800, 1290), (0, 124, 174))
        temp_file       = tempfile.NamedTemporaryFile(suffix = '.jpg')
        image_item.save(temp_file, format = 'JPEG')
        with open(temp_file.name, 'rb') as file_obj:
            data = {
                'contetn':None,
                'image':file_obj
            }
            url = api_reverse('api-status:list')
            response = self.client.post(url, data, format = 'multipart')
            print(response.data)
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        temp_img_dir = os.path.join(settings.MEDIA_ROOT, 'status', 'DDDAA')
        if os.path.exists(temp_img_dir):
            shutil.rmtree(temp_img_dir)
    

    # def test_status_update(self):
    #     data = self.create_item()
    #     data_id  = data.get("id")
    #     put_url  = api_reverse('api-status:detail', kwargs={"id":data_id})
    #     rud_data = {
    #         'content':"another new content"
    #     }
        

    #     '''
    #         get method / retrieve 
    #     '''
    #     rud_response = self.client.put(put_url, data = rud_data, format = 'json')
    #     self.assertEqual(rud_response.status_code, status.HTTP_200_OK)
        
    def test_status_no_create(self):
        data = {
            'content':'Some cool test content'
        }
        url = api_reverse('api-status:list')
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    
    # def test_register_user_api_fail(self):
    #     url  = api_reverse('api-auth:register')
    #     data = {
    #         'username': 'cfe.doe',
    #         'email'   : 'cfe.doe@gmail.com',
    #         'password': 'learncode'
    #     }
    #     response = self.client.post(url, data, format = 'json')
    #     self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    #     self.assertEqual(response.data['password2'][0], "This field is required.")

    # def test_register_user_api(self):
    #     url  = api_reverse('api-auth:register')
    #     data = {
    #         'username': 'cfe.doe',
    #         'password': 'learncode',
    #         'password2': 'learncode',
    #         'email'   : 'bla@gmail.com'
    #     }
    #     response = self.client.post(url, data, format = 'json')
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     token        = response.data.get("token", 0)
    #     token_len    = len(token)
    #     self.assertGreater(token_len, 0)

    # def test_login_user_api(self):
    #     url  = api_reverse('api-auth:login')
    #     data = {
    #         'username': 'DDDAA',
    #         'password': '123abcde'
    #     }
    #     response = self.client.post(url, data, format = 'json')
    #     print(response.data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     token        = response.data.get("token")
    #     token_len    = len(token)
    #     self.assertGreater(token_len, 0)
    
    # def test_login_user_api_fail(self):
    #     url  = api_reverse('api-auth:login')
    #     data = {
    #         'username': 'fdfd',
    #         'password': '123abcde'
    #     }
    #     response = self.client.post(url, data, format = 'json')
    #     print(response.data)
    #     self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
    #     token        = response.data.get("token")
    #     token_len    = 0
    #     if token is not None: 
    #         token_len = len(token)
    #     self.assertEqual(token_len, 0)
    
    # def test_token_login_api(self):
    #     url  = api_reverse('api-auth:login')
    #     data = {
    #         'username': 'DDDAA',
    #         'password': '123abcde'
    #     }
    #     response = self.client.post(url, data, format = 'json')
    #     print(response.data)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     token        = response.data.get("token")
    #     self.client.credentials(HTTP_AUTHORIZATION = 'JWT ' + token)
    #     response2 = self.client.post(url, data, format = 'json')
    #     self.assertEqual(response2.status_code, status.HTTP_403_FORBIDDEN)

    
