from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import StatusSerializer
from status.models import Status 
from rest_framework import generics, mixins, permissions
from django.shortcuts import get_object_or_404
from updates.api.utility import is_json
from rest_framework.authentication import SessionAuthentication
from accounts.api.permissions import IsOwnerOrReadOnly

import json 

# # CreateModelMixin -- handling post 
# # UpdateModelMixin -- Put data 
# # DestroyModelMixin -- DELETE method

class StatusAPIDetailView(
    mixins.UpdateModelMixin, 
    mixins.DestroyModelMixin, 
    generics.RetrieveAPIView):
    
    permission_classes      = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    # authentication_classes  = [SessionAuthentication]
    queryset                = Status.objects.all()
    serializer_class        = StatusSerializer
    lookup_field            = 'id'
    passed_id               = None

    def put(self, request, *args, **kwargs):
        self.passed_id = self.request.GET.get('id') or kwargs["id"]
        self.update(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        self.passed_id = self.request.GET.get('id') or kwargs["id"]
        return super().get(request, *args, **kwargs)

    def get_object(self):
        obj = None 

        if self.passed_id is None:
            self.passed_id = self.request.GET.get('id')

        if self.passed_id is not None: 
            queryset = self.get_queryset()
            obj = get_object_or_404(queryset, id = self.passed_id)
            self.check_object_permissions(self.request, obj)

        return obj
    
    def patch(self, request, *args, **kwargs):
        self.passed_id = self.request.GET.get('id') or kwargs["id"]
        self.update(request, args, kwargs)

    def delete(self, request, *args, **kwargs):
        self.passed_id = self.request.GET.get('id') or kwargs["id"]
        self.destroy(request, args, kwargs)

    # def perform_update(self, serializer):  
    #     pass

class StatusAPIView(
    mixins.CreateModelMixin, 
    generics.ListAPIView):  # Create List
    permission_classes      = [permissions.IsAuthenticatedOrReadOnly]
    # how you would be logged in 
    # authentication_classes  = [SessionAuthentication]
    queryset                = Status.objects.all()
    serializer_class        = StatusSerializer
    passed_id               = None
    search_fields           = ('user__username', 'content', 'user__email')
    gueryset                = Status.objects.all()
    # for ordering the filtering
    ordering_fields         = ('user__username', 'timestamp')

    def post(self, request, *args, **kwargs):
        return self.create(request, args, kwargs)

    def perform_create(self, serializer):
        serializer.save(user = self.request.user)

    # def get(self, request, *args, **kwargs):
    #     print("Get called")
    #     url_passed_id       = request.GET.get('id', None)
    #     json_data           = {}
    #     body_               = request.body 
    #     if is_json(body_):
    #         json_data       = json.loads(body_)
    #     new_passed_id   = json_data.get("id", None)
    #     passed_id = url_passed_id or new_passed_id or None
    #     self.passed_id = passed_id
    #     if passed_id is not None:
    #          return self.retrieve(request, *args, **kwargs)
    #     return super().get(request, *args, **kwargs)

    # def perform_destroy(self):
    #     return 

    # def get_object(self):
    #     print("Get_object called")
    #     request         = self.request 
    #     passed_id       = request.GET.get('id', None) or self.passed_id
    #     queryset        = self.get_queryset()
    #     obj = None
    #     print(passed_id)
    #     if passed_id is not None: 
    #         obj = get_object_or_404(queryset, id = passed_id)
    #         self.check_object_permissions(request, obj)
    #     return obj  

    # def put(self, request, *args, **kwargs):
    #     url_passed_id       = request.GET.get('id', None)
    #     json_data           = {}
    #     body_               = request.body 
    #     if is_json(body_):
    #         json_data       = json.loads(body_)
    #     new_passed_id   = json_data.get("id", None)
    #     requested_id = request.data.get("id")
    #     print("REQUESTED ID " + str(requested_id))
    #     passed_id = url_passed_id or new_passed_id or requested_id or None
    #     self.passed_id = passed_id
    #     return self.update(request, *args, **kwargs)

    # def patch(self, request, *args, **kwargs):
    #     url_passed_id       = request.GET.get('id', None)
    #     json_data           = {}
    #     body_               = request.body 
    #     if is_json(body_):
    #         json_data       = json.loads(body_)
    #     new_passed_id   = json_data.get("id", None)
    #     passed_id = url_passed_id or new_passed_id or None
    #     self.passed_id = passed_id
    #     return self.update(request, args, kwargs)

    # def delete(self, request, *args, **kwargs):
    #     url_passed_id       = request.GET.get('id', None)
    #     json_data           = {}
    #     body_               = request.body 
    #     if is_json(body_):
    #         json_data       = json.loads(body_)
    #     new_passed_id   = json_data.get("id", None)
    #     passed_id = url_passed_id or new_passed_id or None
    #     self.passed_id = passed_id
    #     return self.destroy(request, args, kwargs)

# class StatusListSearchAPIView(APIView):
#     permission_classes      = []
#     authentication_classes  = []

#     def get(self, request, format = None):
#         qs = Status.objects.all()
#         serializer = StatusSerializer(qs, many = True)
#         return Response(serializer.data)

#     def post(self, request, format = None):
#         qs = Status.objects.all()
#         serializer = StatusSerializer(qs, many = True)
#         return Response(serializer.data)


#     # Called by CreateModelMixin when saving a new object instance.
#     # def perform_create(self, serializer):
#     #     serializer.save(user = self.request.user)




# # similar version to below one 
# # class StatusDetailAPIView(mixins.DestroyModelMixin, mixins.UpdateModelMixin, generics.RetrieveAPIView):
# #     permission_classes      = []
# #     authentication_classes  = []
# #     queryset                = Status.objects.all() 
# #     serializer_class        = StatusSerializer

# #     def put(self, request, *args, **kwargs):
# #         return self.update(request, *args, **kwargs)

# #     def delete(self, request, *args, **kwargs):
# #         return self.destroy(request, *args, **kwargs)

# class StatusDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
#     permission_classes      = []
#     authentication_classes  = []
#     queryset                = Status.objects.all() 
#     serializer_class        = StatusSerializer

#     # def get_object(self, *args, **kwargs):
#     #     kwargs = self.kwargs
#     #     kw_id = kwargs.get('id')
#     #     return Status.objects.get(id = kw_id)



