from django.test import TestCase
from django.contrib.auth import get_user_model

User = get_user_model()

# Create your tests here.
class UserTestCase(TestCase):
    
    def setUp(self):
        user = User.objects.create(username = 'DDDAA', email = "dfads@gmail.com")
        user.set_password('123abcde')
        user.save()
    
    def test_created_user(self):
        qs   = User.objects.filter(username = 'DDDAA')
        self.assertEqual(qs.count(), 1)

