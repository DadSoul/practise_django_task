
from django.conf.urls import url,include
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from .views import UserDetailView, UserStatusView

urlpatterns = [
    url(r'^(?P<username>\w+)/$', UserDetailView.as_view(), name = 'detail'),
    url(r'^(?P<username>\w+)/status/$', UserStatusView.as_view(), name = 'status-list'),
]

    














