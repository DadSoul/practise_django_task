
from rest_framework import serializers
from django.contrib.auth import get_user_model
from status.models import Status
from rest_framework.reverse import reverse as api_reverse

User = get_user_model()

class StatusInlineUserSerializer(serializers.ModelSerializer):
    uri                 = serializers.SerializerMethodField(read_only = True)
    class Meta: 
        model = Status
        fields = [
            'uri',
            'id', 
            'user',
            'content',
            'image'
        ]
        read_only_fields = ['user'] # GET  

    def get_uri(self, obj):
        return api_reverse("api-status:detail", kwargs = {"id":obj.id}, request = self.context.get('request'))
       # return f'api/status/{obj.id}/'

class UserDetailSerializer(serializers.ModelSerializer):
    uri             = serializers.SerializerMethodField(read_only = True)
    status          = serializers.SerializerMethodField(read_only = True)
    # statuses        = serializers.HyperlinkedRelatedField(
    #                                                     source = 'status_set', # Status.objects.filter(user=user)
    #                                                     many = True, 
    #                                                     read_only = True, 
    #                                                     lookup_field = 'id', 
    #                                                     view_name = 'api-status:detail'
    #                                                 )
    # alternative to above 
    statuses        = StatusInlineUserSerializer(source = 'status_set',many = True, read_only = True)
    class Meta : 
        model = User
        fields = [
            'id',
            'username', 
            'uri', 
            'status', 
            'statuses'
        ]

    def get_recent(self, obj):
        # returns all status related objects to user model instance
        qs = obj.status_set.all().order_by("-timestamp")[:10] # Status.objects.filter(user=obj)
        return StatusInlineUserSerializer(qs, many = True).data

    def get_uri(self, obj):
        request = self.context.get('request')
        return api_reverse("api-user:detail", kwargs = {'username':obj.username}, request=request)
        #return '/api/users/{id}/'.format(id = obj.id)
    
    def get_status(self, obj):
        request = self.context.get('request')
        limit = 10
        if request: 
            limit_query = request.GET.get('limit')
            try: 
                limit = int(limit_query)
            except:
                pass
        qs = obj.status_set.all().order_by("-timestamp")
        data = {
            'last': StatusInlineUserSerializer(qs.first(), context = {'request': request}).data,
            'recent': StatusInlineUserSerializer(qs[:limit],many = True, context = {'request': request}).data,
            'uri': self.get_uri(obj)
        }
        return data

    def get_status_uri(self, obj):
        return self.get_uri(obj) + "status/"

