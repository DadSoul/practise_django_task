from rest_framework import permissions, generics, pagination
from .serializers import UserDetailSerializer
from django.contrib.auth import authenticate, get_user_model
from status.api.serializers import StatusSerializer
from .serializers import StatusInlineUserSerializer
from status.models import Status
from django.shortcuts import get_object_or_404
from restapi.restconf.pagination import CFEAPIPagination
from status.api.views import StatusAPIView
from rest_framework.response import Response

User = get_user_model() 

class UserDetailView(generics.RetrieveAPIView):
    # by defauly, set up like that
    #permissions_classes         = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class            = UserDetailSerializer 
    queryset                    = User.objects.filter(is_active = True)
    lookup_field                = 'username'

    def get_serializer_context(self):
        return {'request':self.request}

class UserStatusView(StatusAPIView):  
    serializer_class            = StatusInlineUserSerializer 
    def get_queryset(self, *args, **kwargs):
        username = self.kwargs.get('username', None)
        if username is None: 
            return Status.objects.none()
        return Status.objects.filter(user__username = username)

    # def post(self, request, *args, **kwargs):
    #     return Response({"detail":"not allowed"}, status = 400)

        
        