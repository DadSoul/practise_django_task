from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse as api_reverse
from rest_framework import status


User = get_user_model()

# Create your tests here.
class UserAPITestCase(APITestCase):
    
    def setUp(self):
        user = User.objects.create(username = 'DDDAA', email = "dfads@gmail.com")
        user.set_password('123abcde')
        user.save()
    
    def test_created_user_std(self):
        qs   = User.objects.filter(username = 'DDDAA')
        self.assertEqual(qs.count(), 1)

    def test_register_user_api_fail(self):
        url  = api_reverse('api-auth:register')
        data = {
            'username': 'cfe.doe',
            'email'   : 'cfe.doe@gmail.com',
            'password': 'learncode'
        }
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['password2'][0], "This field is required.")

    def test_register_user_api(self):
        url  = api_reverse('api-auth:register')
        data = {
            'username': 'cfe.doe',
            'password': 'learncode',
            'password2': 'learncode',
            'email'   : 'bla@gmail.com'
        }
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        token        = response.data.get("token", 0)
        token_len    = len(token)
        self.assertGreater(token_len, 0)

    def test_login_user_api(self):
        url  = api_reverse('api-auth:login')
        data = {
            'username': 'DDDAA',
            'password': '123abcde'
        }
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token        = response.data.get("token")
        token_len    = len(token)
        self.assertGreater(token_len, 0)
    
    def test_login_user_api_fail(self):
        url  = api_reverse('api-auth:login')
        data = {
            'username': 'fdfd',
            'password': '123abcde'
        }
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        token        = response.data.get("token")
        token_len    = 0
        if token is not None: 
            token_len = len(token)
        self.assertEqual(token_len, 0)
    
    def test_token_login_api(self):
        url  = api_reverse('api-auth:login')
        data = {
            'username': 'DDDAA',
            'password': '123abcde'
        }
        response = self.client.post(url, data, format = 'json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        token        = response.data.get("token")
        self.client.credentials(HTTP_AUTHORIZATION = 'JWT ' + token)
        response2 = self.client.post(url, data, format = 'json')
        self.assertEqual(response2.status_code, status.HTTP_403_FORBIDDEN)

    
