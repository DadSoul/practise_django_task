import requests 
import json 
import os



BASE_URL = "http://127.0.0.1:8000/"
ENDPOINT = "api/status/"
AUTH_ENDPOINT = "api/auth/"
REFRESH_ENDPOINT = AUTH_ENDPOINT + "refresh/"
REGISTER_ENDPOINT = AUTH_ENDPOINT + "register/"
image_path = os.path.join(os.getcwd(), 'spider_two.png')

def do_img(method='get', data = {}, id = 7, is_json = True, img_path = None):
    headers = {}
    if is_json: 
        headers['content-type'] = 'application/json'
        data = json.dumps(data)
        
    if img_path is not None:
        with open(image_path, 'rb') as image:
            file_data = {
                'image':image
            } 
            r = requests.request(method, (BASE_URL + ENDPOINT), data = data, files = file_data)
    else: 
        r = requests.request(method, (BASE_URL + ENDPOINT), data = data, headers = headers)

    print(r.text)
    print(r.status_code)
    return r

def do(method='get', data = {}, id = 7, is_json = True):
    headers = {}
    if is_json: 
        headers['content-type'] = 'application/json'

    if is_json: 
        data = json.dumps(data)
    r = requests.request(method, (BASE_URL + ENDPOINT), data = data, headers = headers)
    print(r.text)
    print(r.status_code)
    return r



# BASE_URL = "http://127.0.0.1:8000/"
# ENDPOINT = "api/status/"
# AUTH_ENDPOINT = "api/auth/"
# REFRESH_ENDPOINT = AUTH_ENDPOINT + "refresh/"
# REGISTER_ENDPOINT = AUTH_ENDPOINT + "register/"
# image_path = os.path.join(os.getcwd(), 'spider_two.png')

# def do_img(method='get', data = {}, id = 7, is_json = True, img_path = None):
#     headers = {}
#     if is_json: 
#         headers['content-type'] = 'application/json'
#         data = json.dumps(data)
        
#     if img_path is not None:
#         with open(image_path, 'rb') as image:
#             file_data = {
#                 'image':image
#             } 
#             r = requests.request(method, (BASE_URL + ENDPOINT), data = data, files = file_data)
#     else: 
#         r = requests.request(method, (BASE_URL + ENDPOINT), data = data, headers = headers)

#     print(r.text)
#     print(r.status_code)
#     return r

# def do(method='get', data = {}, id = 7, is_json = True):
#     headers = {}
#     if is_json: 
#         headers['content-type'] = 'application/json'

#     if is_json: 
#         data = json.dumps(data)
#     r = requests.request(method, (BASE_URL + ENDPOINT), data = data, headers = headers)
#     print(r.text)
#     print(r.status_code)
#     return r

# get_end_point = BASE_URL + ENDPOINT + str(12)
# post_data = json.dumps({"content": "Some random content"})
# r = requests.get(get_end_point)
# print(r.text)

# r2 = requests.get(BASE_URL + ENDPOINT)
# print(r2.status_code)

# post_headers = {
#     'content': "application/json"
# }

# post_response = requests.post((BASE_URL + ENDPOINT), data = post_data, headers = post_headers)
# print(post_response.text)

# do(data = {'id':7})
# do(method = 'delete', data = {'id':7})
# do(method = 'delete', data = {'id':8})


data = {
    'username': "kk5",
    'password':"1q2w3e4r5t6y", 
}  

headers = {
    'content-type':"application/json",
}

r = requests.post((BASE_URL + AUTH_ENDPOINT), data = json.dumps(data), headers = headers)
response = r.json()
token = response['token']
print(token)
print("-"*40)

headers = {
    # 'content-type':"application/json",
    'Authorization': "JWT " + token 
}
  
data_2  = {
    'content': "This new content post"
}


with open(image_path, 'rb') as image:
    file_data = {
        'image':image
    } 
    r = requests.get((BASE_URL + ENDPOINT + str(14) + '/'), headers = headers)
    print(r.text)


# headers = {
#     #'content-type':"application/json",
#     'Authorization': "JWT " + token
#     }

# with open(image_path, 'rb') as image:
#     file_data = {cd
#         'image':image
#     } 
#     # post_data = {}
#     post_data = {
#         "content":"AAA"
#     }

#     posted_response = requests.post((BASE_URL + ENDPOINT), data = post_data, headers = headers, files = file_data)
#     print(posted_response.text)


# new_response = requests.post((BASE_URL + REFRESH_ENDPOINT), data = json.dumps(refresh_data), headers = headers)
# new_token = new_response.json()["token"]
# print(new_token)





