import requests # http requests 
import json

BASE_URL = "http://127.0.0.1:8000/"
ENDPOINT = "api/updates/"

def get_list(id = None): 
    if id is not None:
        data = json.dumps({"id":id})
    else: 
        data = json.dumps({})
    r = requests.get(BASE_URL + ENDPOINT, data = data)
    data = r.json() # converst to python list or dictionary from json string 
    return data

# python treats JSON data as string
# 
def create_update():  

    new_data = {
        'user':1,
        'content':"Akezhan is the best"
    }

    r = requests.post(BASE_URL + ENDPOINT, data = json.dumps(new_data))
    
    print(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text

def update_obj(object_id = 1): 
    new_data = {
        'content':"DadSoul is the best in the world", 
        'id': str(object_id)
        # 'id': str(object_id)
    }
    r = requests.put(BASE_URL + ENDPOINT, data = json.dumps(new_data))
    print(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text

def delete_obj(object_id = 4): 
    new_data = {
        'id':str(object_id)
    }
    r = requests.delete(BASE_URL + ENDPOINT, data = json.dumps(new_data))
    print(r.status_code)
    if r.status_code == requests.codes.ok:
        return r.json()
    return r.text

# print(update_obj(object_id = 4))
# print(create_update())
print(get_list(id = "233"))
